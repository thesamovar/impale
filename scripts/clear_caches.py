from impale import *
from impale.reports import generate_figure, generate_catalogue_replotter

impale_metadata.clear()
impale_metrics.clear()
generate_figure.clear()
generate_catalogue_replotter.clear()
