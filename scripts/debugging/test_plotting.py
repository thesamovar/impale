from dfmgtools import *
from impale import *

#fname = 'DFMG_A91_007-1-1-TCR.mat'
#fname = 'DFMG_A91_007-5-4-MTF_SAM.mat'
#fname = 'DFMG_A91_015-4-1-RL.mat'
#fname = 'a40_s27-9-2-TNINall.mat'
#fname = 'a3_s17-3-2-AZ.mat' # doesn't work at the moment, no SCL.t
#fname = 'DFMG_A91_014-6-3-Pulse.mat'
fname = 'DFMG_A91_023-4-3-JMPT_vary_rate.mat'
data = ImpaleData(fname)

figure(figsize=(14, 10))

#data.plot('allsingleaxis')

#data.plot('rate/raster')

#data.plot('raster', ax=subplot(211))
#data.plot('rate')#, ax=subplot(212))
#data.plot(RatePlotter(plot_kwds={'c':'r'}))
#data.plot('PeriodHistogram', mode='image', ax=subplot(211))
#data.plot('PeriodHistogram', mode='bars', ax=subplot(212))
#data.plot('PSTH', mode='image', ax=subplot(211))
#data.plot('PSTH', mode='bars', ax=subplot(212))
#data.plot('vectorstrength', min_spikes=20)
#data.plot('meanfirstspiketimes', ax=subplot(211))
#data.plot('stdfirstspiketimes', ax=subplot(212))
#data.plot('meanspiketimes', ax=subplot(211))
#data.plot('stdspiketimes', ax=subplot(212))
#data.plot('tuningcurve')
#data.plot('isihistogram', var=())
#data.plot('periodhistogram')
#data.plot('sac')
data.plot('raster')
show()
