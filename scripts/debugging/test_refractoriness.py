from dfmgtools import *
from impale import *

allmd = all_impale_metadata()

refrac = {}

for unit, md in allmd.iteritems():
    if 'session' not in md or 'unit' not in md:
        continue
    key = (md['session'], md['unit'])
    if key in refrac:
        continue
    refrac[key] = estimate_unit_refractoriness(unit)

hist(array(refrac.values())/ms, 100)
show()
