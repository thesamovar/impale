from dfmgtools import *
from impale import *

data = ImpaleData('DFMG_A91_007-5-4-MTF_SAM.mat')

SCL = data.data['SCL']
n, _ = SCL.shape
tdata = SCL['t']
chdata = SCL['ch']
inner_idx_data = SCL['innerIndex']
outer_idx_data = SCL['outerIndex']
rep_idx_data = SCL['repIndex']
inner_outer_trials = {}
trials = []
max_num_reps = amax(rep_idx_data)[0, 0]
for i in xrange(n):
    if not len(inner_idx_data[i, 0].flatten()):
        continue
    inner_idx = inner_idx_data[i, 0][0, 0]-1
    outer_idx = outer_idx_data[i, 0][0, 0]-1
    rep_idx = rep_idx_data[i, 0][0, 0]-1
    if (inner_idx, outer_idx) not in inner_outer_trials:
        t = data.data['t'][inner_idx, outer_idx][:, 0]*ms
        ch = data.data['ch'][inner_idx, outer_idx][:, 0]
        
        isync, = (ch==0).nonzero()
        ispike, = ch.nonzero()
        ts = zeros(len(t))
        ts[isync] = diff(hstack((0, t[isync])))
        t -= cumsum(ts)
        t = t[ispike]
        
        i = cumsum(ch==0)-1
        i = i[ispike]
        
        curtrials = get_trains_from_spikes(i, t, imax=max_num_reps)
        inner_outer_trials[inner_idx, outer_idx] = curtrials
    iot = inner_outer_trials[inner_idx, outer_idx]
    if rep_idx<len(iot):
        trials.append(iot[rep_idx])

for i, (t1, t2) in enumerate(zip(trials, data.trials)):
    if len(t1)!=len(t2.t):
        print 'dsfdslfsd', i, len(t1), len(t2.t), t1, t2.t
        continue
    if not len(t1):
        continue
    if not amax(abs(t1-t2.t))<1e-10:
        print 'rah;', i, t1, t2.t

