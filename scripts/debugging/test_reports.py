from dfmgtools import *
from impale import *

names = [
        'DFMG_A91_015-5-1-MTF_SAM_noise.mat',
        
        'DFMG_A91_014-6-8-MTF_SAM_noise.mat',
        'DFMG_A91_014-6-7-RiseFallL.mat',
    ]

generate_report(names, 'test_report')
