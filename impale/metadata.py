from dfmgtools import *
from data import ImpaleData, get_fname_mappings
from collections import defaultdict
import os

__all__ = ['impale_metadata',
           'impale_merged_metadata',
           'impale_metrics',
           'all_impale_metadata',
           'all_impale_merged_metadata',
           ]

def parse_param(param, ifmt=None):
    try:
        n = len(param)
        if n==0:
            return ''
        return str(param)
    except:
        if param is None:
            return ''
        if ifmt is not None:
            return ifmt % param
        else:
            return str(param)


@func_cache
def impale_metadata(fname, basefolder='~/Data'):
    try:
        data = ImpaleData(fname, basefolder=basefolder)
        return {'filename': fname,
                'fname': fname,
                'session': parse_param(data.info.expName, '%03d'),
                'unit': parse_param(data.info.Unit, '%02d'),
                'suffix': parse_param(data.measParam.Suffix),
                'measurement': parse_param(data.info.measNum, '%03d'),
                'rating': parse_param(data.info.spikeRating),
                }
    except:
        return {}
    
@func_cache
def impale_metrics(fname, basefolder='~/Data'):
    try:
        data = ImpaleData(fname, basefolder=basefolder)
        return dict((k, v) for k, v in data.metrics.__dict__.items() if not k.startswith('_') and (not isinstance(v, ndarray) or len(v)))
    except:
        return {}


def all_impale_metadata(basefolder='~/Data', metadatafunc=impale_metadata):
    fname_map = get_fname_mappings(basefolder)
    allmd = {}
    for fname in fname_map.keys():
        allmd[fname] = metadatafunc(fname)
    return allmd


def impale_merged_metadata(fname, basefolder='~/Data'):
    if basefolder not in all_merged_metadata:
        all_merged_metadata[basefolder] = build_merged_metadata(basefolder)
    merged_metadata = all_merged_metadata[basefolder]
    try:
        return merged_metadata[fname.lower()]
    except:
        return {}


def all_impale_merged_metadata(basefolder='~/Data'):
    return all_impale_metadata(basefolder, impale_merged_metadata)

all_merged_metadata = {}

def build_merged_metadata(basefolder='~/Data'):
    merged_metadata = {}
    allmd = all_impale_metadata(basefolder)
    cells = defaultdict(list)
    for fname, md in allmd.iteritems():
        if 'session' not in md or 'unit' not in md:
            continue
        cell_key = (md['session'], md['unit'])
        cells[cell_key].append(md)
    for cell_key in cells.keys():
        cells[cell_key] = sorted(cells[cell_key], key=lambda md: (md['measurement'], md['filename']))
    for cell_key, cell in cells.iteritems():
        session, unit = cell_key
        merged_metadata[cell_key] = {'session': session, 'unit': unit}
        for md in cell:
            metrics = impale_metrics(md['fname'], basefolder)
            merged_metadata[cell_key].update(metrics)
    for fullname, base, f in pattern_walk(os.path.join(basefolder, 'Metadata'), '*.py'):
        md = {}
        exec open(fullname, 'r').read() in md
        md = dict((k, v) for k, v in md.items() if not k.startswith('_'))
        if not 'session' in md or not 'unit' in md:
            continue
        cell_key = (md['session'], md['unit'])
        merged_metadata[cell_key].update(md)
    for cell_key, cell in cells.iteritems():
        for md in cell:
            merged_metadata[md['fname']] = merged_metadata[cell_key]
    return merged_metadata

if __name__=='__main__':
    fname = 'DFMG_A91_024-3-1-TCR.mat'#.lower()
    #merged_metadata = build_merged_metadata()
    #print merged_metadata['DFMG_A91_024', '03']
    #print merged_metadata[fname]
    #print impale_merged_metadata(fname)
    print all_impale_merged_metadata()[fname.lower()]
    
