from dfmgtools import *
from scipy.io import loadmat
import os
from collections import namedtuple, defaultdict
import cgi

__all__ = ['ImpaleData', 'ImpaleTuningCurveData', 'ImpaleSequencedData',
           'impale_unit_map']

all_fname_mappings = defaultdict(dict)
fname_mappings = all_fname_mappings['*.mat']

def get_fname_mappings(basefolder, pattern='*.mat'):
    fname_mappings = all_fname_mappings[pattern]
    if basefolder in fname_mappings:
        return fname_mappings[basefolder]
    fmap = {}
    for fullname, base, f in pattern_walk(basefolder, pattern):
        fmap[f.lower()] = fullname
    fname_mappings[basefolder] = fmap
    return fmap

def get_fname(fname, basefolder):
    fmap = get_fname_mappings(basefolder)
    return fmap[fname.lower()]

# Standard units for Impale variable names
impale_unit_map = defaultdict(lambda:1.0)
impale_unit_map.update(
    Frequency=Hz,
    dB_SPL=dB,
    modFreq=Hz,
    Pulse_Rate=Hz,
    PPS=Hz,
    )


class ImpaleData(object):
    def __init__(self, fname, basefolder='~/Data'):
        if not fname.lower().endswith('.mat'):
            fname = fname+'.mat'
        raw_fname = get_fname(fname, basefolder)
        self.raw_fname = raw_fname
        self.fname = fname
        self.basefolder = basefolder
        self.data = data = loadmat(raw_fname)

        #################### Analyse the data, extract more useful stuff ###################
        autokeys = set([k for k in data.keys() if not k.startswith('_')])
        exclude = set(['t', 'ch', 'SCL'])
        autokeys.difference_update(exclude)
        for k in autokeys:
            setattr(self, k, structured_object_from_matlab_variable(data[k]))
            
        ################### Test what sort of data this is #################################
        if 'SCL' in data:
            self.__class__ = ImpaleSequencedData
        elif 'TCspl' in data:
            self.__class__ = ImpaleTuningCurveData
        else:
            raise ValueError("Unknown Impale data type")
            
        self._extract_data()
            
    def _extract_data(self):
        pass
    
    def get_plotter(self, plotclass=None, **kwds):
        from impale.plotting import RasterPlotter, ImpalePlotter
        if plotclass is None:
            plotclass = RasterPlotter
        elif isinstance(plotclass, str):
            plotclass = plotclass.lower().replace('plotter', '')
            from impale.plotting.plotter import ImpalePlotter, inheritors
            impale_plotter_classes = inheritors(ImpalePlotter)
            plotclasses = dict((cl.__name__.replace('Plotter', '').lower(), cl) for cl in impale_plotter_classes)
            if plotclass not in plotclasses:
                raise KeyError("Unknown plot type %s." % plotclass)
            plotclass = plotclasses[plotclass]
        if isinstance(plotclass, ImpalePlotter):
            plotter = plotclass
        else:
            plotter = plotclass(**kwds)
        return plotter
    
    def can_plot(self, plotclass=None, **kwds):
        if isinstance(plotclass, str) and '/' in plotclass:
            return True
        plotter = self.get_plotter(plotclass, **kwds)
        return plotter.valid_for(self)

    def plot(self, plotclass=None, fig=None, ax=None, **kwds):
        '''
        Plot using the given plotter object, class, or name. If using a name,
        e.g. RasterPlotter will match to 'raster' and so forth. If using an object,
        the kwds will be ignored.
        '''
        if isinstance(plotclass, str) and '/' in plotclass:
            if fig is None:
                fig = gcf()
            initplotters = plotters = [p.lower().strip() for p in plotclass.split('/')]
            plotters = [plotter for plotter in plotters if self.can_plot(plotter)]
            if len(plotters)==0:
                return
            nr, nc = auto_subplot_dimensions(len(plotters), aspect_min=0.65, prefer_horizontal=False)
            for i, plotter in enumerate(plotters):
                plotter = self.get_plotter(plotter)
                plotter.plot(self, ax=fig.add_subplot(nr, nc, i+1))
            fig.tight_layout()
        else:
            plotter = self.get_plotter(plotclass, **kwds)
            if not plotter.valid_for(self):
                raise ValueError("Plotter cannot plot this data.")
            return plotter.plot(self, fig=fig, ax=ax)

    def summary_html(self, filter='', complete_html=True, details=True, header_level=2):
        if details:
            detailed_info = ''
            newstructobj = StructuredObject()
            for sec in ['adcInt', 'dacInt', 'darwin', 'discrimSettings', 'info', 'innerSeq', 'measParam',
                        'metrics', 'outerSeq', 'respChans', 'seekerParam', 'stimChans']:
                if hasattr(self, sec):
                    setattr(newstructobj, sec, getattr(self, sec))
            newstructobj = newstructobj.apply_filter(filter=filter)
            detailed_info += '''
            <pre>{details}</pre>
            '''.format(details=cgi.escape(repr(newstructobj)))
        
        metrics_table = ''
        for k, v  in self.metrics.__dict__.items():
            if not k.startswith('_') and (not isinstance(v, ndarray) or len(v)):
                metrics_table += '<tr><th align="left">{k}</th><td>{v}</td></tr>'.format(k=k, v=v)
        
        try:
            seq_vars = ''
            for vartype, varname in self.vars.items():
                if varname=='None':
                    continue
                channels = str(self.var_sequence[vartype]['channels'][0])
                seq_vars += '<tr><th align="left">{vartype}</th><td>{varname} (channels {channels})</td></tr>'.format(
                             vartype=vartype, varname=varname, channels=channels)
        except AttributeError:
            pass
        
        stim_chans = ''.join([chr(ord('A')+i) for i in xrange(len(self.stimChans)) if self.stimChans[i].IsActive])
               
        html = '''
        <table>
        <tr><th align="left">Name</th><td>{data.info.expName}</td></tr>
        <tr><th align="left">Filename</th><td>{data.raw_fname}</td></tr>
        <tr><th align="left">Type</th><td>{data.info.expType}</td></tr>
        <tr><th align="left">Date</th><td>{data.info.date}</td></tr>
        <tr><th align="left">Rating</th><td>{data.info.spikeRating}</td></tr>
        <tr><th align="left">P/U/M/R</th><td>
            {data.info.Pass} : {data.info.Unit} : {data.info.measNum} : {data.info.run}
        </td></tr>
        {metrics_table}
        {seq_vars}
        <tr><th align="left">Active channels:</th><td>{stim_chans}</td></tr>
        </table>
        '''.format(data=self, metrics_table=metrics_table,
                   seq_vars=seq_vars, stim_chans=stim_chans)
        if details:
            html = '''
            <h{n}>Summary</h{n}>
            {html}
            <h{n}>Detailed info</h{n}>
            {detailed_info}
            '''.format(html=html, detailed_info=detailed_info, n=header_level)
        if complete_html:
            html = '''
            <head><title>Impale unit</title></head><body>
            {html}
            </body></html>
            '''.format(html=html)
        return html
    

class ImpaleTuningCurveData(ImpaleData):
    '''
    '''
    def _extract_data(self):
        data = self.data
        self.CF = data['CF']*Hz
        self.Threshold = data['Threshold']*dB


class ImpaleSequencedData(ImpaleData):
    '''
    Has the following attributes:
    
    ``data``
        The raw Matlab data file
    ``spikes``
        A namedtuple with fields i, t and each of the variables. For each spike, i is the trial
        number, t is the spike time relative to the start of the trial, and each of the variables
        in the sequence has a corresponding name (see ``vars``).
    ``trials``
        The sequence of trials. Each trial is a namedtuple with fields t (an array of spike times
        for that trial) and each of the variables in the sequence (see ``vars``).
    ``vars``
        A dictionary ``(var_type, var_name)`` where ``var_type`` can be one of: ``inner_master``,
        ``inner_slave1``, ``inner_slave2``, ``inner_slave3``, ``outer_master``, ``outer_slave1``,
        ``outer_slave2``, ``outer_slave3``.
    ``sequence_vars``
        A set of which variable names are part of the inner or outer sequence. 
    
    In addition, all of the Impale variables such as ``metrics``, ``adcInt``, etc. can be
    accessed by, e.g., ``.metrics.Threshold``.
    '''
    def _extract_data(self):
        data = self.data
        #################### Analyse the data, extract more useful stuff ###################
        autokeys = set([k for k in data.keys() if not k.startswith('_')])
        exclude = set(['t', 'ch', 'SCL'])
        autokeys.difference_update(exclude)
        for k in autokeys:
            setattr(self, k, structured_object_from_matlab_variable(data[k]))
        ##### Extract trials and spikes ############
        SCL = data['SCL']
        n, _ = SCL.shape
        # No longer use tdata and chdata because they do not work on older Impale files
        #tdata = SCL['t']
        #chdata = SCL['ch']
        inner_idx_data = SCL['innerIndex']
        outer_idx_data = SCL['outerIndex']
        rep_idx_data = SCL['repIndex']
        max_num_reps = amax(rep_idx_data)[0, 0]
        # Candidate variable names to store in spikes/trials, some of these will be None so we
        # will ignore them
        varcandidates = {
            'inner_master': (SCL['innerIndex'], data['innerSeq'][0,0]['master'][0,0]),
            'inner_slave1': (SCL['innerIndex'], data['innerSeq'][0,0]['slave'][0,0]),
            'inner_slave2': (SCL['innerIndex'], data['innerSeq'][0,0]['slave'][0,1]),
            'inner_slave3': (SCL['innerIndex'], data['innerSeq'][0,0]['slave'][0,2]),
            'outer_master': (SCL['outerIndex'], data['outerSeq'][0,0]['master'][0,0]),
            'outer_slave1': (SCL['outerIndex'], data['outerSeq'][0,0]['slave'][0,0]),
            'outer_slave2': (SCL['outerIndex'], data['outerSeq'][0,0]['slave'][0,1]),
            'outer_slave3': (SCL['outerIndex'], data['outerSeq'][0,0]['slave'][0,2]),
            }
        # Extract variable names corresponding to specifications above, discard those whose
        # variable name is None.
        append_channel = False
        for _ in xrange(2):
            self.sequence_vars = set([])
            vars = {}
            varnames = {}
            self.var_sequence = {}
            origvarnames = {}
            for k, (index, seq) in varcandidates.items():
                var = origvar = seq['var'][0]
                if append_channel and seq['channels']:
                    var = var+'_'+str(seq['channels'][0])
                var = var.replace(' ', '_').replace('-', '_').replace('+', '_')
                origvarnames[var] = origvar
                varnames[k] = var
                self.var_sequence[k] = seq
                if var!='None':
                    vars[k] = (index, seq, var)
                    self.sequence_vars.add(var)
            thevarnames = [var for _, _, var in vars.values()]
            if len(set(thevarnames))==len(thevarnames):
                break
            append_channel = True
        # Create namedtuple types for spikes and trials
        Spikes = namedtuple('Spikes', ['i', 't']+thevarnames)
        Trial = namedtuple('Trial', ['t']+thevarnames)
        # Go through the trials extracting spikes and trials
        alli = []
        allt = []
        vals = defaultdict(list)
        self.trials = trials = []
        inner_outer_trials = {}
        for i in xrange(n):
            if not len(inner_idx_data[i, 0].flatten()):
                continue
            inner_idx = inner_idx_data[i, 0][0, 0]-1
            outer_idx = outer_idx_data[i, 0][0, 0]-1
            rep_idx = rep_idx_data[i, 0][0, 0]-1
            if outer_idx<0:
                outer_idx = 0
            if (inner_idx, outer_idx) not in inner_outer_trials:                    
                t = data['t'][inner_idx, outer_idx][:, 0]*ms
                ch = data['ch'][inner_idx, outer_idx][:, 0]
                
                jsync, = (ch==0).nonzero()
                jspike, = ch.nonzero()
                ts = zeros(len(t))
                ts[jsync] = diff(hstack((0, t[jsync])))
                t -= cumsum(ts)
                t = t[jspike]
                
                j = cumsum(ch==0)-1
                j = j[jspike]
                
                curtrials = get_trains_from_spikes(j, t, imax=max_num_reps)
                inner_outer_trials[inner_idx, outer_idx] = curtrials
            iot = inner_outer_trials[inner_idx, outer_idx]
            if rep_idx>=len(iot):
                continue
            t = iot[rep_idx]
            alli.append(i*ones(len(t)))
            allt.append(t)
            trialvals = {}
            for k, (index, seq, var) in vars.iteritems():
                trialvals[var] = float(seq['values'][0, :][index[i, 0][0, 0]-1])*impale_unit_map[var]
                vals[var].append(trialvals[var]*ones(len(t)))
            trials.append(Trial(t=t, **trialvals))
        i = hstack(alli)
        t = hstack(allt)
        for k, v in vals.items():
            vals[k] = hstack(v)
        self.spikes = Spikes(i=i, t=t, **vals)
        self.vars = varnames
        
    def trials_matching(self, **args):
        '''
        All trials matching a condition.
        
        Give a set of keyword arguments of the form, e.g. ``frequency=f``.
        
        Returns (matching, other_vars) where matching is a list of trials matching
        the conditions in the keyword arguments, and other_vars is a dictionary of
        (varname, values) where the varname are the variables you didn't specify but
        were varying in the sequence, and the values are the set of values for which
        there are trials with that value.
        '''
        names = set(args.keys())
        for name in names:
            if name not in self.sequence_vars:
                raise ValueError("%s not a sequence variable" % name)
        matching = []
        for trial in self.trials:
            ismatch = True
            for name, val in args.iteritems():
                if not float(getattr(trial, name))==float(val):
                    ismatch = False
            if ismatch:
                matching.append(trial)
        gathervars = self.sequence_vars-names
        other_vars = defaultdict(set)
        for trial in matching:
            for var in gathervars:
                other_vars[var].add(getattr(trial, var))
        for k, v in other_vars.items():
            other_vars[k] = sorted(array(list(v)))
        return matching, other_vars
        