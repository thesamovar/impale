'''
Generate a report from a set of units.
'''

from pylab import *
from dfmgtools.core import ensure_directory
from dfmgtools.results.reports import *
from dfmgtools import func_cache, AxisRecorder
from .data import ImpaleData
from .metadata import impale_metadata
from .plotting import valid_impale_plotters
from collections import defaultdict
from brian.utils.progressreporting import ProgressReporter
from matplotlib.backends.backend_pdf import PdfPages
import os

__all__ = ['generate_report']

@func_cache(ignore=['data', 'fig'])
def generate_figure(suname, unitname, plot_type, data, fig):
    fig.clf()
    items = []
    try:
        data.plot(plot_type, fig=fig)
    except Exception as e:
        items.append('Plot error (%s): %s' % (e.__class__.__name__, str(e)))
    F = Figure('%s/%s/%s' % (suname, unitname, plot_type), fig)
    items.append(F)
    return F, items


def chunks(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]


@func_cache(ignore=['fig'])
def generate_catalogue_replotter(unitname, plottype, fig, figsize, sub_h, sub_w, i):
    ax = fig.add_subplot(sub_h, sub_w, i%(sub_w*sub_h)+1)
    rax = AxisRecorder(ax)
    data = ImpaleData(unitname)
    data.plot(plottype, ax=rax)
    return rax._ax_record_replotter()


def generate_report(unitnames, basedir,
                    summary_plot_types='allsingleaxis',
                    restrict_plot_types=None,
                    figsize=(14, 10),
                    summary_pdf_pagesize=(8.3, 11.7),
                    summary_pdf_subplot_layout=(4, 2),
                    summary_pdf_top=0.92,
                    summary_pdf_suppress_axis_labels=True,
                    summary_pdf_sharex=True,
                    summary_pdf_sharey=True,
                    ):
    '''
    - unitnames should be a list of filenames
    - basedir should be the (relative) output directory to put the HTML/image files
    - summary_plot_types can be a single string or list of strings, giving the plot types to
      show in the single page overview
    - restrict_plot_types if specified gives a set of plot types to allow, otherwise use all
      possible plot types
    - figsize is the figure size (see matplotlib.figure command)
    '''
    if isinstance(summary_plot_types, str):
        summary_plot_types = [summary_plot_types]
    # Generate report
    report = Report()
    # Generate figures and summaries
    fig = figure(figsize=figsize)
    all_figs = defaultdict(dict)
    all_summary = {}
    full_summary = {}
    plot_types = {}
    metadata = {}
    sameunitname = {}
    sameunitkey = {}
    sameunits = defaultdict(list)
    print 'Generating figures and summaries'
    progress = ProgressReporter()
    progress.start()
    all_single_axis_plot_types = set()
    for i, unitname in enumerate(unitnames):
        progress.update(float(i)/len(unitnames))
        data = ImpaleData(unitname)
        md = metadata[unitname] = impale_metadata(unitname)
        suname = sameunitname[unitname] = '%s.%s' % (md['session'], md['unit'])
        sameunitkey[suname] = (md['session'], md['unit'])
        sameunits[suname].append(unitname)
        all_summary[unitname] = data.summary_html(details=False, complete_html=False)
        full_summary[unitname] = data.summary_html(details=True, complete_html=False,
                                                   header_level=3)
        if restrict_plot_types is None:
            plot_types[unitname] = valid_impale_plotters(data).keys()
        else:
            plot_types[unitname] = restrict_plot_types
        plot_types[unitname].extend(summary_plot_types)
        plot_types[unitname] = [pt for pt in plot_types[unitname] if data.can_plot(pt)]
        plot_types[unitname] = list(set(plot_types[unitname]))
        for pt in plot_types[unitname]:
            if pt in all_single_axis_plot_types:
                continue
            plotter = data.get_plotter(pt)
            if plotter.single_axis:
                all_single_axis_plot_types.add(pt)
        for plot_type in plot_types[unitname]:
            F, items = generate_figure(suname, unitname, plot_type, data, fig)
            report.add(F)
            all_figs[unitname][plot_type] = items
    progress.finish()
    # generate multipage PDFs
    print 'Generating catalogue PDFs'
    progress = ProgressReporter()
    progress.start()
    pdf_dir = os.path.join(basedir, 'pdf_catalogues')
    ensure_directory(pdf_dir)
    catalogue_links = []
    for i_pt, plottype in enumerate(all_single_axis_plot_types):
        progress.equal_subtask(i_pt, len(all_single_axis_plot_types))
        curunits = []
        for i, unitname in enumerate(unitnames):
            if plottype in plot_types[unitname]:
                curunits.append(unitname)
        if len(curunits)==0:
            continue
        sub_h, sub_w = summary_pdf_subplot_layout
        pages = chunks(list(enumerate(curunits)), sub_w*sub_h)
        pp = PdfPages(os.path.join(pdf_dir, plottype+'.pdf'))
        figs = []
        for pagenum, page in enumerate(pages):
            fig = figure(figsize=summary_pdf_pagesize)
            figs.append(fig)
            for i, unitname in page:
                pos_y = (i%(sub_w*sub_h))/sub_w
                pos_x = (i%(sub_w*sub_h))%sub_w
                progress.update(float(i)/len(curunits))
                replotter = generate_catalogue_replotter(unitname, plottype, fig,
                                                         summary_pdf_pagesize, sub_h, sub_w, i)
                kwds = {}
                if summary_pdf_sharex and i>0:
                    kwds['sharex'] = ax
                if summary_pdf_sharey and i>0:
                    kwds['sharey'] = ax
                ax = fig.add_subplot(sub_h, sub_w, i%(sub_w*sub_h)+1, **kwds)
                ax.clear()
                replotter(ax)
                if i>0:
                    leg = ax.legend()
                    if leg is not None:
                        ax.legend().set_visible(False)
                ax.set_title(unitname)
                if summary_pdf_suppress_axis_labels:
                    if pos_x!=0:
                        ylabel('')
                    if pos_y!=sub_h-1:
                        xlabel('')
            fig.suptitle(plottype+' (page %s of %s)' % (pagenum+1, len(pages)))
            fig.tight_layout()
            fig.subplots_adjust(top=summary_pdf_top)
        for fig in figs:
            pp.savefig(fig)
        pp.close()
        for fig in figs:
            close(fig)
        catalogue_links.append(link(plottype, 'pdf_catalogues/'+plottype+'.pdf'))
    catalogues_page = Page('catalogues', 'Catalogues', UnorderedList(catalogue_links))
    report.add(catalogues_page)
    progress.finish()
    # individual reports
    figpage = {}
    detailspage = {}
    for unitname in unitnames:
        suname = sameunitname[unitname]
        figpage[unitname] = Page(suname+'/'+unitname+'/all_figures', 'All figures for '+unitname,
                                 [all_summary[unitname]])
        figpage[unitname].add(all_figs[unitname].values())
        detailspage[unitname] = Page(suname+'/'+unitname+'/details', 'All details for '+unitname,
                                     [full_summary[unitname]])
        report.add([figpage[unitname], detailspage[unitname]])
    # single page report
    contents_sec = Section('Contents', [])
    single_page_report = Page('single_page_report', 'Single page report', [contents_sec])
    allunits_list = []
    for suname in sorted(sameunits.keys()):
        table = Table([])
        curunit_list = []
        for unitname in sorted(sameunits[suname], key=lambda unitname: metadata[unitname]['measurement']):
            md = metadata[unitname]
            link_allfig = link('all figures', figpage[unitname])
            mtitle = 'Measurement %s: %s (%s)' % (md['measurement'], md['suffix'], link_allfig)
            cur_measurement = TableHeaderRow(anchor(unitname)+mtitle)
            link_meas = link('%s: %s' % (md['measurement'], md['suffix']), '#'+unitname)
            curunit_list.append('%s (%s)' % (link_meas, link_allfig))
            table.add(cur_measurement)
            table.add([
                all_summary[unitname]+'<div>'+link('Click for details', detailspage[unitname])+'</div>',
                [all_figs[unitname][plot_type] for plot_type in summary_plot_types],
                ])
        sec = Section('%sSession %s unit %s (%s)' % ((anchor(suname),)+
                                                     sameunitkey[suname]+
                                                     (link('all pages', suname+'/index.html'),)),
                      table)
        allunits_list.append(link(suname, '#'+suname)+str(UnorderedList(curunit_list)))
        single_page_report.add(sec)
    contents_sec.add(UnorderedList(allunits_list))
    report.add(single_page_report)
    # and write
    ensure_directory(basedir)
    print 'Writing data'
    report.write(basedir)
    print 'Finished'
