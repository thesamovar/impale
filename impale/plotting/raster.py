from dfmgtools import *
from .plotter import ImpalePlotter
from ..data import *
from mpl_toolkits.axes_grid1 import AxesGrid

__all__ = ['RasterPlotter']

class RasterPlotter(ImpalePlotter):
    '''
    A quick raster plot with trials on the y-axis sorted by variables
    '''
    
    single_axis = True
    
    def __init__(self, var=None, var_grid=None, plot_kwds=None, max_ylabels=5, matching=None, rasterized=False):
        self.var = var
        self.var_grid = var_grid
        self.plot_kwds = plot_kwds
        self.max_ylabels = max_ylabels
        if matching is None:
            matching = {}
        self.matching = matching
        self.rasterized = rasterized
        
    def valid_for(self, data):
        if not isinstance(data, ImpaleSequencedData):
            return False
        if self.var_grid is not None and self.var_grid not in data.sequence_vars:
            return False
        return self.var is None or self.var in data.sequence_vars
    
    def plot(self, data, fig=None, ax=None):
        var = self.var
        var_grid = self.var_grid
        plot_kwds = self.plot_kwds
        max_ylabels = self.max_ylabels
        
        if len(data.sequence_vars)-len(self.matching)>=2:
            var, var_grid = self.get_2d_sequence_vars(data, var, var_grid)
        elif var is None:
            var = data.vars['inner_master']
        
        ax = self.get_single_ax(fig, ax)
        fig = ax.figure
        n1, n2, n3 = ax.get_subplotspec().get_geometry()[:3]
        ax_pos = (n1, n2, n3+1)
        
        if var_grid is None:
            trials, vars = data.trials_matching(**self.matching)
            self._plot_trials(ax, trials, data, var, do_label=True)
        else:
            trials, vars = data.trials_matching(**self.matching)
            # by default, use the smaller set of variables for the grid
            if self.var_grid is None and len(vars[var_grid])>len(vars[var]):
                var, var_grid = var_grid, var
            var_grid_vals = vars[var_grid]
            ax.set_xticks([])
            ax.set_yticks([])
            ax.set_ylabel('%s\n%s\n\n' % (var_grid, var), multialignment='center')
            ax.set_xlabel('\n\nTime (ms)')
            g = AxesGrid(fig, ax_pos, (len(var_grid_vals), 1), aspect=False, axes_pad=0.0, share_all=True)
            for i, gval in enumerate(var_grid_vals):
                curax = g[len(var_grid_vals)-1-i]
                m = {var_grid: gval}
                m.update(self.matching)
                trials, _ = data.trials_matching(**m)
                self._plot_trials(curax, trials, data, var, do_label=False)
                curax.set_yticks([])
                curax.set_ylabel(str(gval))

    def _plot_trials(self, ax, trials, data, var, do_label=True):
                    
        sortvals = array([getattr(trial, var) for trial in trials])
        I = argsort(sortvals)
        t = []
        vcur = nan
        jcur = 0
        boxes = []
        for j, i in enumerate(I):
            trial = trials[i]
            v = getattr(trial, var)
            if isnan(vcur) or v!=vcur:
                if not isnan(vcur):
                    boxes.append((jcur, j, vcur, v))
                vcur = v
                jcur = j
            t.append(trial.t)
        boxes.append((jcur, j+1, vcur, v))
        
        tmin = 0
        tmax = data.dacInt.TotalDuration
        
        i, t = get_spikes_from_trains(t)
        dobox = True
        for jcur, j, vcur, v in boxes:
            if dobox:
                ax.fill([tmin, tmax, tmax, tmin], [jcur, jcur, j, j], color=(0.9,)*3)
            dobox = not dobox

        plot_kwds2 = self.get_plot_kwds(ms=3, marker='.', color='k', ls='None')        
        ax.plot(t/ms, i, **plot_kwds2)
        ax.axis('tight')
        ax.set_xlim(tmin, tmax)
        if do_label:
            ax.set_xlabel('Time (ms)')
        for chan in data.stimChans:
            if chan.Gate.Width==0:
                continue
            offset = chan.Gate.Delay+chan.Gate.Width
            onset = chan.Gate.Delay
            onsetramped = onset+chan.Gate.RiseFallTime
            offsetramped = offset-chan.Gate.RiseFallTime
            ax.axvline(offset, ls='--', color='r')
            ax.axvline(onset, ls='--', color='g')
            ax.axvline(offsetramped, ls=':', color='r')
            ax.axvline(onsetramped, ls=':', color='g')

        V = array([v for _, _, v, _ in boxes])
        J = [0.5*(j0+j1) for j0, j1, _, _ in boxes]
        
        if do_label:
            self.auto_ylabel(data, var, J, V, ax)
            ax.set_title('Raster')
            
        if self.rasterized:
            ax.set_rasterized(True)
