from dfmgtools import *

__all__ = ['ImpalePlotter',
           'AllSingleAxisPlotter',
           'all_impale_plotters', 'valid_impale_plotters',
           'fake_axis',
           ]

class ImpalePlotter(object):
    
    single_axis = False
    
    max_xlabels = 5
    max_ylabels = 5
    
    def valid_for(self, data):
        pass
    
    def plot(self, data, fig=None, ax=None):
        pass
    
    def get_unit(self, data, var):
        return get_unit(getattr(data.trials[0], var))
    
    def get_var_axis_label(self, data, var):
        u = self.get_unit(data, var)
        s = var
        if not have_same_dimensions(u, 1):
            s += ' (%s)' % str(u)
        return s
    
    def get_single_ax(self, fig=None, ax=None):
        if fig is None and ax is None:
            ax = gca()
            fig = ax.figure
        elif ax is None:
            ax = fig.add_subplot(111)
        return ax
    
    def get_plot_kwds(self, **defaults):
        plot_kwds = self.plot_kwds
        if plot_kwds is None:
            plot_kwds = {}
        plot_kwds2 = defaults.copy()
        plot_kwds2.update(plot_kwds)
        return plot_kwds2
    
    def auto_xlabel(self, data, var, points, values, ax):
        self._auto_xylabel(data, var, points, values, ax,
                           ax.set_xlabel, ax.set_xticks, ax.set_xticklabels, self.max_xlabels)

    def auto_ylabel(self, data, var, points, values, ax):
        self._auto_xylabel(data, var, points, values, ax,
                           ax.set_ylabel, ax.set_yticks, ax.set_yticklabels, self.max_ylabels)

    def _auto_xylabel(self, data, var, points, values, ax,
                      set_label, set_ticks, set_ticklabels, max_labels):
        s = self.get_var_axis_label(data, var)
        J = points
        V = ['%.1f' % v for v in values]
        if len(V)>max_labels:
            V = V[::len(V)/(self.max_ylabels-1)]
            J = J[::len(J)/(self.max_ylabels-1)]
        set_label(s)
        set_ticks(J)
        set_ticklabels(V)
        
    def get_single_gate(self, gate, data):
        if gate is None or isinstance(gate, str):            
            gates = []
            for curgate in data.measParam.analysisGate:
                if curgate.IsActive=='on':
                    gates.append(curgate)
            if isinstance(gate, str):
                for curgate in gates:
                    if gate==curgate.name:
                        gates = [curgate]
                        break
            if len(gates):
                gate = (gates[0].delay*ms, gates[0].width*ms)
            else:
                gate = (0*second, data.dacInt.TotalDuration*ms)
        return gate
    
    def get_multiple_gates(self, gates, data):
        if gates is None:
            gates = []
            for igate, gate in enumerate(data.measParam.analysisGate):
                if not gate.name:
                    gate.name = 'Gate '+str(igate)
                if gate.IsActive=='on':
                    gates.append((gate.delay*ms, gate.width*ms, gate.name))
            if not len(gates):
                gates = [(0*ms, data.dacInt.TotalDuration*ms, 'all')]
        return gates

    def get_2d_sequence_vars(self, data, varx, vary):
        if varx is not None and vary is not None:
            return varx, vary
        found = set()
        if varx is not None:
            found.add(varx)
        if vary is not None:
            found.add(vary)
        possibles = set(data.sequence_vars)
        possibles -= found
        possibles = sorted(list(possibles))
        possibles.reverse()
        if varx is None:
            varx = possibles.pop()
        if vary is None:
            vary = possibles.pop()
        return varx, vary
        

class AllSingleAxisPlotter(ImpalePlotter):
    '''
    Convenience class plots all single axis plot types
    
    For initialisation arguments, see ``dfmgtools.auto_subplot_dimensions``.
    '''
    def __init__(self, aspect_min=0.65, prefer_horizontal=False):
        self.aspect_min = aspect_min
        self.prefer_horizontal = prefer_horizontal
        
    def valid_for(self, data):
        return True

    def plot(self, data, fig=None, ax=None):
        if fig is None:
            fig = gcf()
        plotters = valid_impale_plotters(data)
        skeys = sorted(plotters.keys())
        plotters = [plotters[k] for k in skeys if plotters[k].single_axis]
        if len(plotters)==0:
            return
        nr, nc = auto_subplot_dimensions(len(plotters), aspect_min=self.aspect_min,
                                         prefer_horizontal=self.prefer_horizontal)
        for i, plotter in enumerate(plotters):
            plotter.plot(data, ax=fig.add_subplot(nr, nc, i+1))
        fig.tight_layout()
    

# Get the inheritors of a class, from StackOverflow
def inheritors(klass):
    subclasses = set()
    work = [klass]
    while work:
        parent = work.pop()
        for child in parent.__subclasses__():
            if child not in subclasses:
                subclasses.add(child)
                work.append(child)
    return subclasses


def all_impale_plotters():
    impale_plotter_classes = inheritors(ImpalePlotter)
    plotclasses = dict((cl.__name__.replace('Plotter', '').lower(), cl) for cl in impale_plotter_classes)
    plotters = dict((k, v()) for k, v in plotclasses.items())
    return plotters


def valid_impale_plotters(data):
    plotters = all_impale_plotters()
    validplotters = {}
    for plotname, plotter in plotters.items():
        if plotter.valid_for(data):
            validplotters[plotname] = plotter
    return validplotters

class FakeAxis(object):
    '''
    Emulates some of an axis object but doesn't plot anything
    '''
    def __getattr__(self, name):
        return self
    def __call__(self, *args, **kwds):
        return self
    def get_view_interval(self):
        return 0, 1

fake_axis = FakeAxis()
