from dfmgtools import *
from .plotter import ImpalePlotter
from ..data import impale_unit_map
from collections import defaultdict
from ..data import *

__all__ = ['VectorStrengthPlotter']

class VectorStrengthPlotter(ImpalePlotter):
    
    single_axis = True
    
    def __init__(self, var=None, gates=None, plot_kwds=None, min_spikes=20, subtract_spont_gate=None,
                 rayleigh=True, rayleigh_p=0.01, return_rayleigh_pval=False,
                 matching=None):
        self.var = var
        self.gates = gates
        self.plot_kwds = plot_kwds
        self.min_spikes = min_spikes
        self.subtract_spont_gate = subtract_spont_gate
        self.matching = matching
        self.rayleigh = rayleigh
        self.rayleigh_p = rayleigh_p
        self.return_rayleigh_pval = return_rayleigh_pval
        
    def valid_for(self, data):
        if not isinstance(data, ImpaleSequencedData):
            return False
        if self.var is None:
            var = data.vars['inner_master']
            return have_same_dimensions(impale_unit_map[var], Hz)
        else:
            return self.var in data.sequence_vars
    
    def plot(self, data, fig=None, ax=None):
        var = self.var
        
        match_base = self.matching
        if match_base is None:
            match_base = {}
        
        if var is None:
            var = data.vars['inner_master']

        gates = self.get_multiple_gates(self.gates, data)
        
        if self.subtract_spont_gate is not None:
            spont_gate = self.get_single_gate(self.subtract_spont_gate, data)
        
        ax = self.get_single_ax(fig, ax)
        plot_kwds = self.get_plot_kwds(ls='-', marker='o')        

        _, other_vars = data.trials_matching(**match_base)
        freqs = other_vars[var]
        all_vs = defaultdict(list)
        all_pval = defaultdict(list)
        for ifreq, freq in enumerate(freqs):
            m = {var: freq}
            m.update(match_base)
            trials, _ = data.trials_matching(**m)
            trials = [trial.t for trial in trials]
            i, t = get_spikes_from_trains(trials)
            for onset, width, gatename in gates:
                s = t[(t>=onset)&(t<onset+width)]
                if self.rayleigh:
                    if len(s)==0:
                        vs = nan
                    else:
                        n = len(s)
                        vs = abs(sum(exp(2*pi*freq*s*1j)))/n
                        # Rayleigh test code comes from:
                        # http://www.mathworks.com/matlabcentral/fileexchange/10676-circular-statistics-toolbox--directional-statistics-
                        R = n*vs
                        z = R*R/n
                        pval = exp(sqrt(1+4*n+4*(n**2-R**2))-(1+2*n))
                        if not self.return_rayleigh_pval:
                            if pval>self.rayleigh_p:
                                vs = nan
                else:
                    if len(s)<self.min_spikes:
                        vs = nan
                    else:
                        if self.subtract_spont_gate is not None:
                            o, w = spont_gate
                            ts = t[(t>=o)&(t<o+w)]
                            n = len(s)-width*len(ts)/w
                        else:
                            n = len(s)
                        vs = abs(sum(exp(2*pi*freq*s*1j)))/n
                all_vs[gatename].append(vs)
                if self.return_rayleigh_pval:
                    all_pval[gatename].append(pval)
                
        for gatename, vs in all_vs.items():
            vs = array(vs)
            all_vs[gatename] = vs
            ax.semilogx(freqs, vs, label=gatename, **plot_kwds)
            
        ax.set_xlim(amin(freqs), amax(freqs))
        log_frequency_xaxis_labels(ax=ax)
        ax.set_xlabel(self.get_var_axis_label(data, var))
        ax.set_ylim(0, 1)
        ax.legend(loc='best')
        ax.set_ylabel("Vector strength")
        
        ax.set_title("Vector strength")
        
        if self.return_rayleigh_pval:
            return freqs, all_vs, all_pval
        else:
            return freqs, all_vs
