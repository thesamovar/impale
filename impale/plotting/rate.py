from dfmgtools import *
from .plotter import ImpalePlotter
from collections import defaultdict
from ..data import *

__all__ = ['RatePlotter']

class RatePlotter(ImpalePlotter):
    
    single_axis = True
    
    def __init__(self, var=None, plot_kwds=None, max_ylabels=5, gates=None,
                 matching=None):
        self.var = var
        self.plot_kwds = plot_kwds
        self.max_ylabels = max_ylabels
        self.gates = gates
        self.matching = matching
        
    def valid_for(self, data):
        if not isinstance(data, ImpaleSequencedData):
            return False
        return self.var is None or self.var in data.sequence_vars
    
    def plot(self, data, fig=None, ax=None):
        var = self.var
        plot_kwds = self.plot_kwds
        max_ylabels = self.max_ylabels
        
        if var is None:
            var = data.vars['inner_master']

        gates = self.get_multiple_gates(self.gates, data)
        
        ax = self.get_single_ax(fig, ax)

        u = self.get_unit(data, var)

        vmin = inf,
        vmax = -inf
        
        lines = {}

        for (onset, width, gatename) in gates:
            counts = defaultdict(float)
            numtrials = defaultdict(float)
            if self.matching is None:
                trials = data.trials
            else:
                trials, _ = data.trials_matching(**self.matching)
            for i, trial in enumerate(trials):
                v = getattr(trial, var)
                t = trial.t
                t = t[(t>=onset)&(t<onset+width)]
                counts[v] += len(t)
                numtrials[v] += 1
            rates = zeros(len(counts))
            values = sorted(counts.keys())
            for i, val in enumerate(values):
                rates[i] = counts[val]/(width*numtrials[val])
    
            plot_kwds = self.get_plot_kwds()
            
            lines[gatename] = (array(values), array(rates))        
            
            if have_same_dimensions(u, Hz):
                ax.semilogx(values, rates, label=gatename, **plot_kwds)
                vmin = min(vmin, amin(values))
                vmax = max(vmax, amax(values))
            else:
                ax.plot(values, rates, label=gatename, **plot_kwds)
        
        if have_same_dimensions(u, Hz):
            ax.set_xlim(vmin, vmax)
            log_frequency_xaxis_labels(ax=ax)
        else:
            axis('tight')
        ax.set_xlabel(self.get_var_axis_label(data, var))
        ax.set_ylabel('Rate over whole trial (sp/s)')
        ax.set_ylim(0)
        ax.legend(loc='best')
        
        ax.set_title('Rate')
        
        return lines
        

class Rate2DPlotter(ImpalePlotter):
    
    single_axis = True
    
    # Default values
    interpolation = 'nearest'
    colorbar = False
    aspect = 'auto'
    
    def __init__(self, varx=None, vary=None, gate=None, **kwds):
        self.varx = varx
        self.vary = vary
        self.gate = gate
        for k, v in kwds.items():
            setattr(self, k, v)
    
    def valid_for(self, data):
        if not isinstance(data, ImpaleSequencedData):
            return False
        if len(data.sequence_vars)<2:
            return False
        if self.varx is not None and self.varx not in data.sequence_vars:
            return False
        if self.vary is not None and self.vary not in data.sequence_vars:
            return False
        return True

    def plot(self, data, fig=None, ax=None):
        varx, vary = self.get_2d_sequence_vars(data, self.varx, self.vary)
        onset, width = gate = self.get_single_gate(self.gate, data)        
        ax = self.get_single_ax(fig, ax)

        counts = defaultdict(float)
        numtrials = defaultdict(float)
        for i, trial in enumerate(data.trials):
            vx = float(getattr(trial, varx))
            vy = float(getattr(trial, vary))
            t = trial.t
            t = t[(t>=onset)&(t<onset+width)]
            counts[vx, vy] += len(t)
            numtrials[vx, vy] += 1
        valuesx, valuesy = zip(*counts.keys())
        valuesx = sorted(unique(valuesx))
        valuesy = sorted(unique(valuesy))
        nx = len(valuesx)
        ny = len(valuesy)
        rates = zeros((nx, ny))
        for ix, vx in enumerate(valuesx):
            for iy, vy in enumerate(valuesy):
                rates[ix, iy] = iy+counts[vx, vy]/(width*numtrials[vx, vy])
        img = ax.imshow(rates.T, interpolation=self.interpolation, origin='lower left',
                        vmin=0, aspect=self.aspect)
        ylabel_points = arange(len(valuesy))+.5
        xlabel_points = arange(len(valuesx))+.5
        if self.colorbar:
            ax.figure.colorbar(img)
            
        self.auto_xlabel(data, varx, xlabel_points, valuesx, ax)
        self.auto_ylabel(data, vary, ylabel_points, valuesy, ax)
        
        ax.set_title('Rate')
            
        return valuesx, valuesy, rates
