from dfmgtools import *
from .plotter import ImpalePlotter
from collections import defaultdict
from ..data import *

__all__ = ['TuningCurvePlotter']

class TuningCurvePlotter(ImpalePlotter):
    
    single_axis = True
    
    def __init__(self, plot_kwds=None):
        self.plot_kwds = plot_kwds
    
    def valid_for(self, data):
        return isinstance(data, ImpaleTuningCurveData)
    
    def plot(self, data, fig=None, ax=None):
        ax = self.get_single_ax(fig, ax)
        plot_kwds = self.get_plot_kwds()        
        
        ax.semilogx(data.TCspl.xval, data.TCspl.thresh, **plot_kwds)
        
        ax.set_xlim(amin(data.TCspl.xval), amax(data.TCspl.xval))
        ax.set_ylim(0, 100)
        
        log_frequency_xaxis_labels(ax=ax)
        
        ax.set_xlabel('Frequency (Hz)')
        ax.set_ylabel('Threshold (dB SPL)')
        
        ax.set_title('Frequency tuning curve')
