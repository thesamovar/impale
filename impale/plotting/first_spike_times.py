from dfmgtools import *
from .plotter import ImpalePlotter
from ..data import impale_unit_map
from collections import defaultdict
from ..data import *

__all__ = ['MeanFirstSpikeTimesPlotter', 'StdFirstSpikeTimesPlotter',
           'MeanSpikeTimesPlotter', 'StdSpikeTimesPlotter',
           ]

class MeanFirstSpikeTimesPlotter(ImpalePlotter):
    
    single_axis = True
    
    def __init__(self, var=None, gates=None, plot_kwds=None, plot_std=False):
        self.var = var
        self.gates = gates
        self.plot_kwds = plot_kwds
        self.plot_std = plot_std
        
    def valid_for(self, data):
        if not isinstance(data, ImpaleSequencedData):
            return False
        if self.var is None:
            return True
        else:
            return self.var in data.sequence_vars
    
    def plot(self, data, fig=None, ax=None):
        var = self.var
        
        if var is None:
            var = data.vars['inner_master']

        gates = self.get_multiple_gates(self.gates, data)
        
        ax = self.get_single_ax(fig, ax)
        if self.plot_std:
            plot_kwds = self.get_plot_kwds()        
        else:
            plot_kwds = self.get_plot_kwds(lw=2, elinewidth=1)        

        _, other_vars = data.trials_matching()
        vals = other_vars[var]
        all_fst = defaultdict(list)
        all_std = defaultdict(list)
        for ival, val in enumerate(vals):
            m = {var: val}
            trials, _ = data.trials_matching(**m)
            trials = [trial.t for trial in trials]
            for onset, width, gatename in gates:
                allt = []
                for t in trials:
                    t = t[(t>=onset)&(t<onset+width)]
                    if len(t):
                        allt.append(t[0]/ms)
                if len(allt):
                    fst = mean(allt)
                    s = std(allt)
                else:
                    fst = nan
                    s = nan
                all_fst[gatename].append(fst)
                all_std[gatename].append(s)

        u = self.get_unit(data, var)
                
        vmin = inf,
        vmax = -inf
        
        if have_same_dimensions(u, Hz):
            ax.set_yscale('log')
                
        for gatename, fst in all_fst.iteritems():
            fst = array(fst)
            s = array(all_std[gatename])
            if self.plot_std:
                ax.plot(s, vals, label=gatename, **plot_kwds)
            else:
                ax.errorbar(fst, vals, xerr=s, label=gatename, **plot_kwds)
            vmin = min(vmin, amin(vals))
            vmax = max(vmax, amax(vals))
            
        if have_same_dimensions(u, Hz):
            ax.set_ylim(vmin, vmax)
            log_frequency_yaxis_labels(ax=ax)
        else:
            axis('tight')
        ax.set_ylabel(self.get_var_axis_label(data, var))
        ax.set_xlim(0)
        ax.legend(loc='best')

        if self.plot_std:
            ax.set_xlabel('Std first spike time (ms)')
            ax.set_title('Std first spike times')
        else:
            ax.set_xlabel('Mean first spike time (ms)')
            ax.set_title('Mean first spike times')
        

class StdFirstSpikeTimesPlotter(MeanFirstSpikeTimesPlotter):
    def __init__(self, var=None, gates=None, plot_kwds=None, plot_std=True):
        MeanFirstSpikeTimesPlotter.__init__(self, var=var, gates=gates, plot_kwds=plot_kwds,
                                            plot_std=plot_std)


class MeanSpikeTimesPlotter(ImpalePlotter):
    
    single_axis = True
    
    def __init__(self, var=None, gate=None, numtimes=5, plot_kwds=None, plot_std=False):
        self.var = var
        self.gate = gate
        self.plot_kwds = plot_kwds
        self.numtimes = numtimes
        self.plot_std = plot_std
        
    def valid_for(self, data):
        if not isinstance(data, ImpaleSequencedData):
            return False
        if self.var is None:
            return True
        else:
            return self.var in data.sequence_vars
    
    def plot(self, data, fig=None, ax=None):
        var = self.var
        numtimes = self.numtimes
        
        if var is None:
            var = data.vars['inner_master']

        onset, width = self.get_single_gate(self.gate, data)
        
        ax = self.get_single_ax(fig, ax)
        if self.plot_std:
            plot_kwds = self.get_plot_kwds()        
        else:
            plot_kwds = self.get_plot_kwds(lw=2, elinewidth=1)        

        _, other_vars = data.trials_matching()
        vals = other_vars[var]
        all_fst = defaultdict(list)
        all_std = defaultdict(list)
        for ival, val in enumerate(vals):
            m = {var: val}
            trials, _ = data.trials_matching(**m)
            trials = [trial.t for trial in trials]
            for spikenum in xrange(numtimes):
                allt = []
                for t in trials:
                    t = t[(t>=onset)&(t<onset+width)]
                    if len(t)>spikenum:
                        allt.append(t[spikenum]/ms)
                if len(allt):
                    fst = mean(allt)
                    s = std(allt)
                else:
                    fst = nan
                    s = nan
                all_fst[spikenum].append(fst)
                all_std[spikenum].append(s)

        u = self.get_unit(data, var)
                
        vmin = inf,
        vmax = -inf
        
        if have_same_dimensions(u, Hz):
            ax.set_yscale('log')
                
        for spikenum, fst in all_fst.iteritems():
            fst = array(fst)
            s = array(all_std[spikenum])
            if self.plot_std:
                ax.plot(s, vals, label=str(spikenum+1), **plot_kwds)
            else:
                ax.errorbar(fst, vals, xerr=s, label=str(spikenum+1), **plot_kwds)
            vmin = min(vmin, amin(vals))
            vmax = max(vmax, amax(vals))
            
        if have_same_dimensions(u, Hz):
            ax.set_ylim(vmin, vmax)
            log_frequency_yaxis_labels(ax=ax)
        else:
            axis('tight')
        ax.set_ylabel(self.get_var_axis_label(data, var))
        ax.set_xlim(0)

        if self.plot_std:
            ax.set_xlabel('Std spike time (ms)')
            ax.set_title('Std spike times')
            ax.legend(loc='best')
        else:                
            ax.set_xlabel('Mean spike time (ms)')
            ax.set_title('Mean spike times')


class StdSpikeTimesPlotter(MeanSpikeTimesPlotter):
    def __init__(self, var=None, gate=None, numtimes=5, plot_kwds=None, plot_std=True):
        MeanSpikeTimesPlotter.__init__(self, var=var, gate=gate, numtimes=numtimes,
                                       plot_kwds=plot_kwds, plot_std=plot_std)

