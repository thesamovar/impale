from dfmgtools import *
from dfmgtools.compute.auditory.shuffled_correlograms import *
from .plotter import ImpalePlotter
from ..data import impale_unit_map
from ..data import *

__all__ = ['HistogramPlotter',
           'PeriodHistogramPlotter',
           'PeriodTriggeredHistogramPlotter',
           'PSTHPlotter',
           'FirstSpikeTimeHistogramPlotter',
           'ISIHistogramPlotter',
           'SACPlotter',
           ]

class HistogramPlotter(ImpalePlotter):
    '''
    mode can be one of 'bars', 'image', None (use default)
    gate can be a string, in which case it will search the known gates, or a pair (onset, width),
    or None in which case it will use the first gate if there is one, or all the spikes if not.
    '''
        
    single_axis = True
    normalise_to_max = True
    spacing = 0.5
    mode = None
    gate = None
    plot_kwds = None
    numbins = 20
    binwidth = 1*ms
    
    def __init__(self, var=None, **kwds):
        self.var = var
        for k, v in kwds.items():
            setattr(self, k, v)
                
    def valid_for(self, data):
        return False # base class only
    
    def _get_bins(self, data, vals):
        pass
    
    def _get_histval(self, t, val):
        pass
    
    def _get_title(self):
        pass
    
    def _get_xlabel(self):
        pass
    
    def plot(self, data, fig=None, ax=None):
        var = self.var
        mode = self.mode
        
        if var is None:
            var = data.vars['inner_master']
        if len(var)==0:
            var = None

        onset, width = self.get_single_gate(self.gate, data)
        
        ax = self.get_single_ax(fig, ax)

        bins = self._get_bins(data, onset, width)
        binmids = 0.5*(bins[1:]+bins[:-1])

        if var is not None:
            _, other_vars = data.trials_matching()
            vals = other_vars[var]
        else:
            vals = [1]
        norm_hists = []
        raw_hists = []
        for val in vals:
            if var is not None:
                m = {var: val}
                trials, _ = data.trials_matching(**m)
            else:
                trials, _ = data.trials_matching()
            trials = [trial.t for trial in trials]
            i, t = get_spikes_from_trains(trials)
            I = (t>=onset)&(t<onset+width)
            t = t[I]
            i = i[I]
            h = self._compute_histogram(i, t, bins, val=val, onset=onset, width=width)
            raw_hists.append(h)
            if var is not None and amax(h)>0:
                if self.normalise_to_max:
                    hnorm = h*1.0/amax(h)
                else:
                    hnorm = h*1.0/sum(h)
            else:
                hnorm = h
            norm_hists.append(hnorm)
        
        if mode is None:
            if len(norm_hists)>10:
                mode = 'image'
            else:
                mode = 'bars'
        if mode=='bars':
            plot_kwds = self.get_plot_kwds(color='k')        
            ylabel_points = []
            for ival, h in enumerate(norm_hists):
                ax.bar(bins[:-1], h,
                       width=bins[1]-bins[0],
                       bottom=(1+self.spacing)*ival, **plot_kwds)
                ylabel_points.append((1+self.spacing)*(ival+0.5))
            ax.set_xlim(amin(bins), amax(bins))
        elif mode=='lines':
            plot_kwds = self.get_plot_kwds(ls='-', color='k')
            ylabel_points = []
            for ival, h in enumerate(norm_hists):
                ax.plot(binmids, h+(1+self.spacing)*(ival+0.5), **plot_kwds)
                ylabel_points.append((1+self.spacing)*(ival+0.5))
            ax.set_xlim(amin(bins), amax(bins))
        elif mode=='image':
            plot_kwds = self.get_plot_kwds(origin='lower left', interpolation='nearest',
                                           aspect='auto')
            img = array(norm_hists)
            ax.imshow(img, extent=(amin(bins), amax(bins), 0, len(norm_hists)), **plot_kwds)
            ylabel_points = arange(len(norm_hists))+.5
        else:
            raise ValueError("Unknown mode: "+mode)
        
        if var is not None:
            self.auto_ylabel(data, var, ylabel_points, vals, ax)
        
        ax.set_xlabel(self._get_xlabel())

        ax.set_title(self._get_title())
        
        return bins, binmids, array(vals), array(raw_hists), array(norm_hists)
        
    def _compute_histogram(self, i, t, bins, **kwds):
        histval = self._get_histval(i, t, **kwds)
        h, _ = histogram(histval, bins)
        return h
        

class PeriodHistogramPlotter(HistogramPlotter):
    normalise_to_max = False
    centralised = True
    
    def valid_for(self, data):
        if not isinstance(data, ImpaleSequencedData):
            return False
        if self.var is not None and len(self.var)==0:
            return False
        if self.var is None:
            var = data.vars['inner_master']
            return have_same_dimensions(impale_unit_map[var], Hz)
        else:
            return self.var in data.sequence_vars

    def _get_bins(self, data, onset, width):
        return linspace(0, 360., self.numbins+1)
    
    def _get_histval(self, i, t, val=None, **kwds):
        freq = val
        if self.centralised:
            phi = 2*pi*freq*t
            meanphi = angle(sum(exp(1j*phi)))
            phi = phi-meanphi
            phi = (phi+3*pi)%(2*pi)
            phi *= 180/pi
            return phi
        else:
            return (360*freq*t)%360
    
    def _get_xlabel(self):
        return 'Phase (deg)'
    
    def _get_title(self):
        return "Period histogram"


class PeriodTriggeredHistogramPlotter(PeriodHistogramPlotter):
    normalise_to_max = True
    duration = 50*ms
    
    def _get_bins(self, data, onset, width):
        numbins = int(self.duration/self.binwidth)
        return linspace(0, self.duration/ms, numbins+1)
    
    def _get_histval(self, i, t, val=None, **kwds):
        freq = val
        duration = self.duration
        period = 1/freq
        n = int(ceil(duration/period))
        allt = []
        for j in xrange(n):
            s = t-j*period
            s = s[(s>=0)&(s<=duration)]
            allt.append(s)
        t = hstack(allt)
        return t/ms
    
    def _get_xlabel(self):
        return 'Time (ms)'
    
    def _get_title(self):
        return "Period-triggered histogram"


class PSTHPlotter(HistogramPlotter):
    def valid_for(self, data):
        if not isinstance(data, ImpaleSequencedData):
            return False
        if self.var is None or len(self.var)==0:
            return True
        else:
            return self.var in data.sequence_vars

    def _get_bins(self, data, onset, width):
        return arange(float(onset/ms), float((onset+width)/ms)+float(self.binwidth/ms), float(self.binwidth/ms))
    
    def _get_histval(self, i, t, **kwds):
        return t/ms
    
    def _get_xlabel(self):
        return 'Time (ms)'
    
    def _get_title(self):
        return "PSTH"


class FirstSpikeTimeHistogramPlotter(PSTHPlotter):
    def _get_histval(self, i, t, **kwds):
        if not len(t):
            return t
        trains = get_trains_from_spikes(i, t)
        return array([t[0] for t in trains if len(t)])/ms

    def _get_title(self):
        return "First spike time histogram"


class ISIHistogramPlotter(PSTHPlotter):
    def __init__(self, var=(), isi_max=5*ms, binwidth=0.1*ms, **kwds):
        PSTHPlotter.__init__(self, var=var, binwidth=binwidth, **kwds)
        self.isi_max = isi_max

    def _get_bins(self, data, onset, width):
        return arange(0, float(self.isi_max/ms)+float(self.binwidth/ms), float(self.binwidth/ms))
        
    def _get_histval(self, i, t, **kwds):
        trains = get_trains_from_spikes(i, t)
        all_isis = []
        for train in trains:
            if len(train)>1:
                isis = diff(train)/ms
                all_isis.append(isis)
        if not len(all_isis):
            return array([])
        all_isis = hstack(all_isis)
        return all_isis

    def _get_xlabel(self):
        return 'ISI (ms)'

    def _get_title(self):
        return "ISI histogram"


class SACPlotter(ISIHistogramPlotter):
    def __init__(self, var=None, isi_max=5*ms, compute_isi_max=None, binwidth=50*usecond,
                 mode='lines', **kwds):
        ISIHistogramPlotter.__init__(self, var=var, isi_max=isi_max, binwidth=binwidth, mode=mode,
                                     **kwds)
        if compute_isi_max is None:
            compute_isi_max = 5*isi_max
        self.compute_isi_max = compute_isi_max

    def _get_bins(self, data, onset, width):
        binwidth = float(self.binwidth/ms)
        isimax = float(self.isi_max/ms)
        return arange(-isimax-binwidth/2, isimax+binwidth, binwidth)

    def _compute_histogram(self, i, t, bins, width=None, **kwds):
        if not len(t):
            return zeros(len(bins)-1)
        binmids, h, h0 = SAC(i, t, 100*Hz, self.isi_max, self.binwidth, width, amax(i)+1)
        I = (binmids>-self.isi_max)&(binmids<self.isi_max)
        h = h[I]
        return h
        
    def _get_title(self):
        return "Shuffled auto-correlogram"
    