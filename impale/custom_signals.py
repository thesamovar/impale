from dfmgtools import *
from dfmgtools.matlab.auditory.wav import read_wav_with_matlab
import os

__all__ = ['get_custom_signal']

fname_mappings = {}

def get_fname_mappings(basefolder):
    basefolder = os.path.expanduser(basefolder)
    if basefolder in fname_mappings:
        return fname_mappings[basefolder]
    fmap = {}
    for (dirpath, dirnames, filenames) in os.walk(basefolder):
        for filename in filenames:
            if not filename.lower().endswith('.wav'):
                continue
            base, f = os.path.split(filename)
            fbase, _ = os.path.splitext(f)
            parts = fbase.split('-')
            name = parts[0]
            fmap[name, frozenset(parts[1:])] = os.path.normpath(os.path.join(dirpath, filename))
    fname_mappings[basefolder] = fmap
    return fmap

@func_cache
def cached_loadwav(fname):
    return read_wav_with_matlab(fname)

def get_custom_signal(name, basefolder='~/Data/Signals', **kwds):
    fmap = get_fname_mappings(basefolder)
    key = (name, frozenset((k+str(int(v))) for k, v in kwds.items()))
    return cached_loadwav(fmap[key])

if __name__=='__main__':
    sound = get_custom_signal('JMPT', pr=20, jitter=0)
    plot(sound.times, sound)
    show()
    