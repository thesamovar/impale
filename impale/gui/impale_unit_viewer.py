from dfmgtools import *
import os
import sys
import re
import multiprocessing

import matplotlib
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from copy import copy

from PyQt4 import QtCore, QtGui
from impale_unit_viewer_ui import Ui_ImpaleUnitViewer
from ..data import *
from ..metadata import *
from ..plotting.plotter import valid_impale_plotters
from ..reports import generate_report

__all__ = ['impale_unit_viewer']

def try_tight_layout(fig):
    try:
        fig.tight_layout()
    except ValueError:
        pass    

class ImpaleUnitViewer(QtGui.QMainWindow):
    def __init__(self, parent=None):
        # Do basic setup from Qt Designer
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_ImpaleUnitViewer()
        self.ui.setupUi(self)
        # Now we add some stuff that Qt Designer can't do
        # TODO: Fix a bug with heights of combo boxes
        # First of all, we want a navigation toolbar for the matplotlib figure
        self.mpl_toolbar = NavigationToolbar(self.ui.mplwidget.figure.canvas, self)
        self.ui.plot_layout.addWidget(self.mpl_toolbar)
        # Next we want to clear the default axis because we will manage this ourselves
        self.figure = self.ui.mplwidget.figure
        self.figure.clear()
        # Create some application variables
        self.filter = ''
        # Default plotter will be RasterPlotter unless there is a default.py
        default = os.path.expanduser('~/.impale/plot_types/default.py')
        if os.path.exists(default):
            self.curplotter = open(default, 'r').read()
        else:
            self.curplotter = 'RasterPlotter'
        # Now we want to populate the tree with some data files
        self.tree_columns = ['session', 'unit', 'measurement', 'suffix', 'rating', 'filename']
        self.all_metadata = dict((k, v) for k, v in all_impale_metadata().items() if len(v))
        self.curgrouping = ['session', 'unit']
        self.populate_tree()
        
    def change_grouping(self, grouping):
        grouping = str(grouping).lower().replace('group by:', '').strip()
        grouping = [group.strip() for group in grouping.split('/')]
        if len(grouping)==1 and grouping[0]=='none':
            grouping = []
        self.curgrouping = grouping
        self.populate_tree()
    
    def populate_tree(self):
        self.tree_nodes = {}
        self.tree_children = {}
        self.ui.unit_tree.clear()
        grouping = self.curgrouping
        columns = copy(grouping)
        for col in self.tree_columns:
            if col not in columns:
                columns.append(col)
        columninds = dict((k, i) for i, k in enumerate(columns))
        self.ui.unit_tree.setHeaderLabels([col.capitalize() for col in columns])
        parents = {}
        for k, v in self.all_metadata.items():
            newcols = [v[c] for c in columns]
            curparent = self.ui.unit_tree.invisibleRootItem()
            treepath = ()
            all_parents = []
            for groupvar in grouping:
                newcols[columninds[groupvar]] = ''
                group = v[groupvar]
                treepath = treepath+(group,)
                if treepath not in parents:
                    cols = ['']*len(columns)
                    cols[columninds[groupvar]] = group
                    item = QtGui.QTreeWidgetItem(cols)
                    curparent.addChild(item)
                    parents[treepath] = item
                curparent = parents[treepath]
                all_parents.append(treepath)
                if treepath not in self.tree_children:
                    self.tree_children[treepath] = (curparent, [])
            newitem = QtGui.QTreeWidgetItem(newcols)
            newitem.fname = k
            curparent.addChild(newitem)
            self.tree_nodes[k] = newitem
            for treepath in all_parents:
                _, children = self.tree_children[treepath]
                children.append(newitem)
        for i in range(len(columns))[::-1]:
            self.ui.unit_tree.sortByColumn(i)
        self.ui.unit_tree.expandAll()
        for i in xrange(len(columns)):
            self.ui.unit_tree.resizeColumnToContents(i)
        self.ui.unit_tree.collapseAll()
        
    def change_filter(self, filter):
        self.filter = str(filter)
        self.update_info()        
                
    def clicked_unit(self, item, someint):
        # Get the new unit
        if not hasattr(item, 'fname'):
            return
        fname = str(item.fname)
        self.curdata = ImpaleData(fname)
        # Select the plotter
        curplotter = self.curplotter
        self.ui.plot_type.clear()
        foundplotter = False
        for plotclass in valid_impale_plotters(self.curdata).values():
            plotter = plotclass.__class__.__name__
            item = self.ui.plot_type.addItem(plotter)
            if plotter==curplotter:
                foundplotter = True
                self.ui.plot_type.setCurrentIndex(self.ui.plot_type.count()-1)
        if not foundplotter:
            if ';' in curplotter or '(' in curplotter or '/' in curplotter:
                self.ui.plot_type.addItem(curplotter)
                self.ui.plot_type.setCurrentIndex(self.ui.plot_type.count()-1)
            else:
                if isinstance(self.curdata, ImpaleTuningCurveData):
                    self.ui.plot_type.setEditText('TuningCurvePlotter')
                    self.curplotter = 'TuningCurvePlotter'
                elif isinstance(self.curdata, ImpaleSequencedData):
                    self.ui.plot_type.setEditText('RasterPlotter')
                    self.curplotter = 'RasterPlotter'
                else:                    
                    self.ui.plot_type.setCurrentIndex(0)
                    self.curplotter = str(self.ui.plot_type.currentText())
        # Draw the data
        self.draw_curdata()
        self.update_info()
        
    def update_info(self):
        # Write out some meta data
        self.ui.unit_info.setHtml(self.curdata.summary_html(filter=self.filter))
        
    def changed_plot_type(self, plotter):
        if not plotter:
            return
        plotter = str(plotter)
        self.curplotter = plotter
        self.draw_curdata()
        
    def save_plot_type(self):
        basedir = os.path.expanduser('~/.impale/plot_types')
        ensure_directory(basedir)
        fname = QtGui.QFileDialog.getSaveFileName(self, 'Save file', basedir, '*.py')
        plotter = str(self.ui.plot_type.currentText())
        self.changed_plot_type(plotter)
        try:
            open(fname, 'w').write('\n'.join(line.strip() for line in plotter.split(';')))
            self.ui.statusbar.showMessage("File saved OK")
        except IOError as e:
            self.ui.statusbar.showMessage("File not saved: "+str(e))
    
    def load_plot_type(self):
        basedir = os.path.expanduser('~/.impale/plot_types')
        ensure_directory(basedir)
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file', basedir, '*.py')
        try:
            plotter = open(fname).read().replace('\n', '; ')
            self.ui.statusbar.showMessage("File loaded OK")
            self.ui.plot_type.setEditText(plotter)
            self.changed_plot_type(plotter)
        except IOError as e:
            self.ui.statusbar.showMessage("File not loaded: "+str(e))
                
    def draw_curdata(self, data=None, figure=None, ax=None):
        if figure is None:
            figure = self.figure
        if data is None and hasattr(self, 'curdata'):
            data = self.curdata
        if ax is None:
            figure.clear()
        single_ax_plot = False
        plotter_obj = None
        try:
            if hasattr(self, 'curdata'):
                doplot = True
                if ';' in self.curplotter:
                    ns = {}
                    exec 'from dfmgtools import *; from impale import *' in ns
                    ns.update(figure=figure,
                              data=data,
                              )
                    exec self.curplotter in ns
                    doplot = False
                elif '(' in self.curplotter:
                    ns = {}
                    exec 'from dfmgtools import *; from impale import *' in ns
                    plotter = eval(self.curplotter, ns)
                    plotter_obj = plotter
                    single_ax_plot = plotter.single_axis
                else:
                    # try splitting into multiple plotters
                    initplotters = plotters = [p.lower().strip() for p in self.curplotter.split('/')]
                    plotters = [plotter for plotter in plotters if data.can_plot(plotter)]
                    if len(initplotters)==1:
                        # otherwise fall back
                        plotter = self.curplotter.replace('Plotter', '').lower()
                        plotter_obj = data.get_plotter(plotter)
                        single_ax_plot = plotter_obj.single_axis
                    else:
                        if len(plotters)>=1 and sum(not re.match(u'^\w+$', p) for p in plotters)==0:
                            n = int(ceil(sqrt(len(plotters))))
                            for i, plotter in enumerate(plotters):
                                data.plot(plotter, ax=figure.add_subplot(n, n, i+1))
                            doplot = False
                        else:
                            raise ValueError("No valid plotters")
                if doplot:
                    if ax is not None:
                        data.plot(plotter, ax=ax)
                    else:
                        data.plot(plotter, fig=figure)
            self.ui.statusbar.showMessage('Plot OK')
        except Exception as e:
            self.ui.statusbar.showMessage("Plot error (%s): %s"%(e.__class__.__name__, str(e)))
        if ax is None:
            try_tight_layout(figure)
            figure.canvas.draw()
        return single_ax_plot, plotter_obj
        
    def get_currently_selected_units(self):
        toprocess = self.ui.unit_tree.selectedItems()
        units = []
        while len(toprocess):
            item = toprocess.pop()
            if hasattr(item, 'fname'):
                units.append(item.fname)
            else:
                for j in xrange(item.childCount()):
                    toprocess.append(item.child(j))
        return units

    def create_report(self):
        units = self.get_currently_selected_units()
        if len(units):
            file = str(QtGui.QFileDialog.getExistingDirectory(self, "Select output directory"))
            ptypes = 'allsingleaxis'
            if ';' not in self.curplotter and '(' not in self.curplotter:
                ptypes = self.curplotter
            multiprocessing.Process(target=generate_report, args=(units, file),
                                    kwargs={'summary_plot_types': ptypes}).start()
    
    def change_selection_mode(self, multiselect):
        if multiselect:
            self.ui.unit_tree.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)
        else:
            self.ui.unit_tree.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
            
    def get_units_string(self):
        lines = ['units = [']
        for unit in self.get_currently_selected_units():
            lines.append("    '%s'," % unit)
        lines.append('    ]')
        unitlist = '\n'.join(lines)
        return unitlist
            
    def copy_units_to_clipboard(self):
        unitlist = self.get_units_string()
        clipboard = QtGui.QApplication.clipboard()
        clipboard.setText(unitlist)
        print unitlist
        
    def save_selection(self):
        basedir = os.path.expanduser('~/.impale/unit_lists')
        ensure_directory(basedir)
        fname = QtGui.QFileDialog.getSaveFileName(self, 'Save unit list', basedir, '*.py')
        unitlist = self.get_units_string()
        try:
            open(fname, 'w').write(unitlist)
            self.ui.statusbar.showMessage("File saved OK")
        except IOError as e:
            self.ui.statusbar.showMessage("File not saved: "+str(e))
    
    def load_selection(self):
        basedir = os.path.expanduser('~/.impale/unit_lists')
        ensure_directory(basedir)
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Open unit list', basedir, '*.py')
        try:
            unitlist = open(fname).read()
            ns = {}
            exec unitlist in ns
            units = ns['units']
            self.ui.statusbar.showMessage("File loaded OK")
            unitset = set([unit.lower() for unit in units])
            self.ui.unit_tree.clearSelection()
            for unit in unitset:
                item = self.tree_nodes[unit]
                item.setSelected(True)
                while item is not None:
                    item.setExpanded(True)
                    item = item.parent()
            # select parents when all children are selected
            for item, children in self.tree_children.values():
                if all([child.fname.lower() in unitset for child in children]):
                    item.setSelected(True)
            
        except IOError as e:
            self.ui.statusbar.showMessage("File not loaded: "+str(e))
        
    def plot_selected_units(self):
        units = self.get_currently_selected_units()
        if len(units):
            fig = figure()
            single_ax, plotter_obj = self.draw_curdata(figure=fig, data=ImpaleData(units[0]))
            close(fig)
            if single_ax and plotter_obj is not None:
                units = [unit for unit in units if plotter_obj.valid_for(ImpaleData(unit))]
                if len(units)==0:
                    return
            ion()
            if single_ax:
                figmax = min(len(units), 9)
                w, h = auto_subplot_dimensions(figmax)
                for i, unit in enumerate(units):
                    if i%figmax==0:
                        if i>0:
                            try_tight_layout(fig)
                        fig = figure()
                    ax = fig.add_subplot(w, h, i%figmax+1)
                    self.draw_curdata(figure=fig, ax=ax, data=ImpaleData(unit))
                    ax.set_title(unit)
                try_tight_layout(fig)
            else:
                for unit in units:
                    fig = figure()
                    self.draw_curdata(figure=fig, data=ImpaleData(unit))
                    fig.suptitle(unit)
                    try_tight_layout(fig)
                    fig.subplots_adjust(top=0.91)
            ioff()

def impale_unit_viewer():
    app = QtGui.QApplication(sys.argv)
    myapp = ImpaleUnitViewer()
    myapp.show()
    sys.exit(app.exec_())
