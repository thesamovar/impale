'''
The idea of this GUI is to select a set of units, either by clicking them, applying a filter,
or generating all like a given unit; and then doing a given plot for each unit, and outputting
a report (HTML directory). Should also be able to load and save lists of units.

The report generation step is in impale.reports.
'''
