__all__ = ['estimate_unit_refractoriness']

from dfmgtools import *
from ..metadata import all_impale_metadata
from ..data import ImpaleData, ImpaleSequencedData

@func_cache
def estimate_unit_refractoriness(unit, p=0.99, fallback=1*ms):
    '''
    Estimate refractoriness of a neuron from ISIs computed across all measurements.
    
    Computes all ISIs and returns the ISI value so that a fraction p of ISIs are larger than this value.
    '''
    allmd = all_impale_metadata()
    unit_md = allmd[unit.lower()]
    cells = []
    for fname, md in allmd.iteritems():
        if 'session' not in md or 'unit' not in md:
            continue
        if md['session']==unit_md['session'] and md['unit']==unit_md['unit']:
            cells.append(md['filename'])
    all_isi = []
    for unit in cells:
        data = ImpaleData(unit)
        if not isinstance(data, ImpaleSequencedData):
            continue
        for trial in data.trials:
            all_isi.append(diff(trial.t))
    if len(all_isi)==0:
        return fallback
    all_isi = hstack(all_isi)
    all_isi.sort()
    try:
        refractoriness = all_isi[int(len(all_isi)*(1-p))]
    except IndexError:
        return fallback
    return refractoriness*second
    