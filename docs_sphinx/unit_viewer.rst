Unit viewer
===========

This is the documentation for the Impale Unit Viewer (``scripts/impale_unit_viewer.py``).
This allows you to view individual ``.mat`` data files in various ways.

Installation
------------

Install Python using the Python(x,y) distribution, then install joblib and this package.

Or, install Anaconda, joblib and the matplotlibwidget file at
``https://xy-27.pythonxy.googlecode.com/hg-history/45dba4317f6994f8bb09f73a13c95745c3c35134/src/python/matplotlib/QtDesigner_Plugins/matplotlibwidget.py``. 

Basic usage
-----------

The window is divided into a left half (control panel) and right half (plot panel). The relative
sizes of these can be adjusted. The plot panel is a standard ``matplotlib`` figure.

The control panel consists of the following elements:

1. **Grouping mode selector**. You can choose to group units in the tree view by any columns.
   Either select from the existing choices, or enter your own choice separated by ``/``.

2. **Unit tree**. This tree view shows all the data files with various columns and groups.
   Clicking on a data file will load it.

3. **Information panel**. This shows some summary information at the top, followed by the
   complete text representation of the ``.mat`` file.

4. **Plot type selector**. This allows you to choose what to plot in the right hand side, and
   save and load custom plot types. See below for more details.
   
File locations
--------------

The unit viewer will find all files in the ``~/Data`` directory, where ``~`` means your user
home directory, e.g. the data directory might be ``C:\Users\goodman\data``. It could be modified
to look in other directories, but for the moment if you want to use a different directory,
create a symbolic link or junction, for Windows you can use `this tool 
<http://technet.microsoft.com/en-us/sysinternals/bb896768.aspx>`_.
   
Plot types
----------

The most complicated part of the viewer is the plot type selector, as there are many different
types of values that you can select here. The simplest is to select from the existing values.
Each time you click a unit, the set of valid plot types for that unit will be loaded in this
list. However, you can write arbitrary plotting code in various formats here, and load and save
these for future quick access. The following types of code can be entered:

1. **Custom single plotter**. For example, you might want to overwrite the default arguments for
   ``RatePlotter`` by writing here ``RatePlotter(max_ylabels=10)``. See individual ``ImpalePlotter``
   classes in ``impale.plotting`` for details on these keywords.

2. **Multiple plotters**. For example, if you want to show a raster plot, rate plot and
   vector strength plot, write ``raster/rate/vectorstrength``. The names are case-insensitive,
   and you do not need to include the ``Plotter`` part of the name. Any plot types that are
   invalid for this data file will be ignored. The dimensions of the plot will be automatically
   selected to try to minimise wasted space without creating aspect ratios that are too weird.
   Note that only plotters that use a single axis can be used with this.
   
3. **Arbitrary code.** For the ultimate in control, enter any valid Python code that can be written
   in one line using semicolons to separate multiple lines. The names ``figure`` and ``data``
   can be used. So for example if you wanted a three panel plot with raster and rate on the top
   row, and period histogram spread across the bottom row, you would write this (only separating
   the lines with semicolons instead of new lines)::
   
      data.plot('raster',ax=figure.add_subplot(221))
      data.plot('rate',ax=figure.add_subplot(222))
      data.plot('periodhistogram',ax=figure.add_subplot(212))

Default plot types
------------------

If you save a plot type with the name ``default`` then it will be selected when you first open
the unit viewer.
