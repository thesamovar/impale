.. Impale documentation master file, created by
   sphinx-quickstart on Wed Aug 14 18:16:40 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Impale's documentation!
==================================

This is the documentation for the Python analysis tools for the Impale data files generated
by Ken Hancock's Impale Matlab suite.

Contents:

.. toctree::
   :maxdepth: 2
   
   unit_viewer


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

